package com.Students;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Student> students = new ArrayList<>();

        students.add(new Student(1000001, "Boris", "Berezovskyi", "Abramovich", 1992,
                "Dnepr", "+385000982", "FAKS", "5", "BA-01"));
        students.add(new Student(2000002, "Oleg", "Lunin", "Petrovich", 1993,
                "Dnepr", "+3805005460982", "FEL", "4", "FA-01"));
        students.add(new Student(20000034, "Nikolay", "Belov", "Abramovich", 1992,
                "Dnepr", "+385000964682", "IEE", "2", "OA-01"));
        students.add(new Student(20000021, "Vitaliy", "Dubin", "Abramovich", 1992,
                "Dnepr", "+3850009565682", "MMI", "5", "VA-01"));
        students.add(new Student(30000041, "Yevgen", "Berezovskyi", "Aah", 1992,
                "Dnepr", "+38500560982", "FL", "6", "EA-01"));
        students.add(new Student(200005501, "Nataliya", "Berezovskaya", "Abramovich", 1989,
                "Dnepr", "+385000677982", "ZF", "4", "ZA-01"));

        System.out.println(students);
    }
}
