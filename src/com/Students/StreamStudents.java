package com.Students;

import java.util.List;
import java.util.stream.Collectors;

public class StreamStudents {

    public static List<Student> studentsByFaculty(List<Student> students,
                                                  String faculty) {
        return students.stream().filter(student -> student.getFaculty()
                .equals(faculty)).collect(Collectors.toList());
    }

    public static List<Student> studentsByFacultyAndCourse(List<Student> students,
                                                           String faculty, String course) {
        return students.stream().filter(student -> student.getFaculty()
                .equals(faculty) && student.getCourse().equals(course))
                .collect(Collectors.toList());
    }

    public static List<Student> studentsByYearOfBirth(List<Student> students,
                                                      int yearOfBirth) {
        return students.stream().filter(student ->
                student.getYearOfBirth() >= yearOfBirth)
                .collect(Collectors.toList());
    }

    public static List<String> studentsByNameInGroup(List<Student> students) {
        return students.stream().map(student -> student.getLastName() + " "
                + student.getFirstName() + " " + student.getPatronymic())
                .collect(Collectors.toList());
    }

    public static List<String> studentsByGroup(List<Student> students) {
        return students.stream().map(student -> student.getLastName() + " "
                + student.getFirstName() + " " + student.getPatronymic()
                + " " + student.getFaculty() + " " + student.getGroup())
                .collect(Collectors.toList());
    }

    public static long studentsByCountOnFaculty(List<Student> students,
                                                String faculty) {
        return students.stream().filter(student ->
                student.getFaculty().equals(faculty)).count();
    }
}
