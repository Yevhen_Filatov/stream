package com.Students;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StreamStudentsTest {

    List<Student> students = new ArrayList<>();

    @BeforeEach
    public void createTestArray() {
        students.add(new Student(1000001, "Boris", "Berezovskyi", "Abramovich", 1997,
                "Dnepr", "+385000982", "FAKS", "5", "BA-01"));
        students.add(new Student(1000002, "Oleg", "Lunin", "Petrovich", 1993,
                "Kharkiv", "+3805005460982", "FEL", "4", "FA-01"));
        students.add(new Student(10000034, "Nikolay", "Belov", "Abramovich", 1992,
                "Kherson", "+385000964682", "IEE", "2", "OA-01"));
        students.add(new Student(10000021, "Vitaliy", "Dubin", "Abramovich", 1992,
                "Dnepr", "+3850009565682", "MMI", "5", "VA-01"));
        students.add(new Student(10000041, "Yevgen", "Berezovskyi", "Aah", 1992,
                "Odessa", "+38500560982", "FL", "6", "EA-01"));
        students.add(new Student(100005501, "Nataliya", "Berezovskaya", "Abramovna", 1989,
                "Kyiv", "+385000677982", "ZF", "4", "ZA-01"));
    }

    @Test
    void showStudentsByFaculty() {

        //given
        String faculty = "ZF";

        //when
        String expected = "[Student{id=100005501, " +
                "firstName=Nataliya, " +
                "lastName=Berezovskaya, " +
                "patronymic=Abramovna, " +
                "yearOfBirth=1989, " +
                "address=Kyiv, " +
                "telephone=+385000677982, " +
                "faculty=ZF, " +
                "course=4, " +
                "group=ZA-01}]";

        List<Student> actual = StreamStudents.studentsByFaculty(students, faculty);

        //then
        assertEquals(expected, String.valueOf(actual));
    }

    @Test
    public void showStudentsByFacultyAndCourse() {

        //given
        String faculty = "ZF";
        String course = "4";

        //when
        String expected = "[Student{id=100005501, " +
                "firstName=Nataliya, " +
                "lastName=Berezovskaya, " +
                "patronymic=Abramovna, " +
                "yearOfBirth=1989, " +
                "address=Kyiv, " +
                "telephone=+385000677982, " +
                "faculty=ZF, " +
                "course=4, " +
                "group=ZA-01}]";


        List<Student> actual = StreamStudents
                .studentsByFacultyAndCourse(students, faculty, course);

        //then
        assertEquals(expected, String.valueOf(actual));
    }

    @Test
    void studentsByYearOfBirth() {
        //given
        int yearOfBirth= 1989;


        //when
        String expected = "[Student{id=100005501, " +
                "firstName=Nataliya, " +
                "lastName=Berezovskaya, " +
                "patronymic=Abramovna, " +
                "yearOfBirth=1989, " +
                "address=Kyiv, " +
                "telephone=+385000677982, " +
                "faculty=ZF, " +
                "course=4, " +
                "group=ZA-01}]";


        List<Student> actual = StreamStudents
                .studentsByYearOfBirth(students, yearOfBirth);

        //then
        assertEquals(expected, String.valueOf(actual));
    }

    @Test
    void studentsByNameInGroup() {
        //given
        String firstName= "Nataliya";


        //when
        String expected = "[Student{id=100005501, " +
                "firstName=Nataliya, " +
                "lastName=Berezovskaya, " +
                "patronymic=Abramovna, " +
                "yearOfBirth=1989, " +
                "address=Kyiv, " +
                "telephone=+385000677982, " +
                "faculty=ZF, " +
                "course=4, " +
                "group=ZA-01}]";

      //  List<Student> actual = StreamStudents
        //        .studentsByNameInGroup(students);

        //then
       // assertEquals(expected, actual);

    }

    @Test
    void studentsByGroup() {
    }

    @Test
    void studentsByCountOnFaculty() {
    }
}